/**
 * @description This Variable defines all metadata for the navbar
 * @type {{layout: {logoHeight: string, sidePaddingDesktop: string}, navigationElements: [string, string, string], logoFile: string, socialIcons: [[string, string], [string, string], [string, string], [string, string], [string, string]], colors: {background: string, text: string}}}
 */
export let config = {
    navigationElements: [
        'About',
        'Team',
        'Contact'
    ],
    // Use classes for <i> Elements. You can include Fontawesome here
    // Format: Class, Link
    socialIcons: [
        ['fab fa-instagram','https://instagram.com'],
        ['fab fa-facebook','https://facebook.com'],
        ['fab fa-pinterest','https://pinterest.com'],
        ['fab fa-xing','https://xing.com'],
        ['fab fa-twitter','https://twitter.com']
    ],
    logoFile: '/logo.svg',
    colors: {
        background: 'rgb(20,20,25)',
        text: 'white'
    },
    layout: {
        sidePaddingDesktop: '200px',
        logoHeight: '60px'
    }
}