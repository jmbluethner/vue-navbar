import * as navbar from './navbar-config.js';

let bus = {};
bus.eventBus = new Vue();

const app = new Vue({
    el: '#app',
    data: {},

    methods: {},
    created: function () {

    },
    components: {
        'vue-navbar': {
            data: function () {
                return {
                    navigationElements: navbar.config.navigationElements,
                    socialIcons: navbar.config.socialIcons
                }
            },
            methods:  {

            },
            created: function() {

            },
            template:
                '<div id="navbar_container" style="background-color: '+navbar.config.colors.background+'; padding: 25px '+navbar.config.layout.sidePaddingDesktop+' 25px '+navbar.config.layout.sidePaddingDesktop+';">' +
                '   <div class="navbar_left">' +
                '       <img class="navbar_logo" style="height: '+navbar.config.layout.logoHeight+'" src="'+navbar.config.logoFile+'" alt="Logo Image">' +
                '   </div>' +
                '   <div class="navbar_middle">' +
                '       <button style="color: '+navbar.config.colors.text+';" v-for="el in navigationElements">{{el}}</button>' +
                '   </div>' +
                '   <div class="navbar_right">' +
                '       <a v-for="icon in socialIcons" :href="icon[1]">' +
                '           <i style="color: '+navbar.config.colors.text+'" :class="icon[0]" class="navbar_social_icon"></i>' +
                '       </a>' +
                '   </div>' +
                '</div>'
        }
    }
});